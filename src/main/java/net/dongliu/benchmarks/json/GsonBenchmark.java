package net.dongliu.benchmarks.json;

import net.dongliu.benchmarks.mock.Company;
import net.dongliu.benchmarks.mock.Mocks;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.HashMap;

/**
 * @author Liu Dong
 */
public class GsonBenchmark {
    @Benchmark
    public void beanDeserialization() {
        Constants.gson.fromJson(Constants.gsonJson, Company.class);
    }

    @Benchmark
    public void rawDeserialization() {
        Constants.gson.fromJson(Constants.gsonJson, HashMap.class);
    }

    @Benchmark
    public void beanSerialization() {
        Constants.gson.toJson(Mocks.company);
    }

    @Benchmark
    public void rawSerialization() {
        Constants.gson.toJson(Mocks.map);
    }
}

package net.dongliu.benchmarks.json;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import net.dongliu.benchmarks.mock.Mocks;

/**
 * @author Dong Liu
 */
class Constants {

    static ObjectMapper mapper = new ObjectMapper();
    static Gson gson = new Gson();

    public static String jacksonJson = jacksonJson(Mocks.company);
    public static String gsonJson = Constants.gson.toJson(Mocks.company);
    public static String fastJson = JSON.toJSONString(Mocks.company);

    private static String jacksonJson(Object o) {
        try {
            return Constants.mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}

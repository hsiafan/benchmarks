package net.dongliu.benchmarks.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.dongliu.benchmarks.mock.Company;
import net.dongliu.benchmarks.mock.Mocks;
import org.openjdk.jmh.annotations.Benchmark;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author Liu Dong
 */
public class JacksonBenchmark {

    @Benchmark
    public void beanDeserialization() throws IOException {
        Constants.mapper.readValue(Constants.jacksonJson, Company.class);
    }

    @Benchmark
    public void rawDeserialization() throws IOException {
        Constants.mapper.readValue(Constants.jacksonJson, HashMap.class);
    }

    @Benchmark
    public void beanSerialization() throws JsonProcessingException {
        Constants.mapper.writeValueAsString(Mocks.company);
    }

    @Benchmark
    public void rawSerialization() throws JsonProcessingException {
        Constants.mapper.writeValueAsString(Mocks.map);
    }
}

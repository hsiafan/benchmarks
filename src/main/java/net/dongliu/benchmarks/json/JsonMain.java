package net.dongliu.benchmarks.json;

import net.dongliu.benchmarks.RunResultExporter;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.Collection;

/**
 * @author Liu Dong
 */
public class JsonMain {
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include("net.dongliu.benchmarks.json.*?Benchmark")
                .forks(1)
                .warmupTime(TimeValue.seconds(1))
                .warmupIterations(3)
                .measurementTime(TimeValue.seconds(2))
                .measurementIterations(5)
                .build();
        Collection<RunResult> result = new Runner(opt).run();

        RunResultExporter.imgPath = "json_benchmarks.png";
        RunResultExporter.title = "Json lib benchmark";
        new RunResultExporter().export(result);
    }
}


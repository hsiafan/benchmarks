package net.dongliu.benchmarks.json;

import com.alibaba.fastjson.JSON;
import net.dongliu.benchmarks.mock.Company;
import net.dongliu.benchmarks.mock.Mocks;
import org.openjdk.jmh.annotations.Benchmark;

/**
 * @author Liu Dong
 */
public class FastJsonBenchmark {

    @Benchmark
    public void beanDeserialization() {
        JSON.parseObject(Constants.fastJson, Company.class);
    }

    @Benchmark
    public void rawDeserialization() {
        JSON.parse(Constants.fastJson);
    }

    @Benchmark
    public void beanSerialization() {
        JSON.toJSONString(Mocks.company);
    }

    @Benchmark
    public void rawSerialization() {
        JSON.toJSONString(Mocks.map);
    }
}

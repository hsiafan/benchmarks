package net.dongliu.benchmarks;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import net.dongliu.commons.collection.Pair;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

/**
 * @author Liu Dong
 */
public class ImageExporter extends Application {

    @Override
    public void start(Stage stage) throws Exception {
//        stage.setTitle("Chart Sample");
        //defining the axes
        final CategoryAxis yAxis = new CategoryAxis();
        final NumberAxis xAxis = new NumberAxis();
        final BarChart<Number, String> barChart = new BarChart<>(xAxis, yAxis);
        barChart.setTitle(RunResultExporter.title);
//        yAxis.setLabel("Role");
        xAxis.setLabel("QPS");

        Scene scene = new Scene(barChart, 800, 600);
        barChart.setAnimated(false);

        for (Pair<String, List<Pair<String, Number>>> category : RunResultExporter.data) {
            XYChart.Series<Number, String> series = new XYChart.Series<>();
            series.setName(category.getKey());
            for (Pair<String, Number> item : category.getSecond()) {
                series.getData().add(new XYChart.Data<>(item.getValue(), item.getKey()));
            }
            barChart.getData().add(series);
        }
        stage.setScene(scene);
//        stage.show();
        saveAsPng(barChart);
        stop();
        Platform.exit();
    }

    private void saveAsPng(Chart chart) {
        WritableImage image = chart.snapshot(new SnapshotParameters(), null);
        File file = new File(RunResultExporter.imgPath);
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void run(String[] args) {
        launch(args);
    }
}
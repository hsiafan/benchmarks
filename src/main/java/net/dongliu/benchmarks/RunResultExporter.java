package net.dongliu.benchmarks;

import net.dongliu.commons.collection.Lists;
import net.dongliu.commons.collection.Pair;
import net.dongliu.commons.collection.Tuple3;
import net.dongliu.commons.collection.Tuples;
import org.openjdk.jmh.infra.BenchmarkParams;
import org.openjdk.jmh.results.Result;
import org.openjdk.jmh.results.RunResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * @author Liu Dong
 */
public class RunResultExporter {

    public static volatile List<Pair<String, List<Pair<String, Number>>>> data;
    public static volatile String imgPath;
    public static volatile String title;

    public void export(Collection<RunResult> result) {
        Map<String, List<Tuple3<String, String, Double>>> map = result.stream().map(runResult -> {
            BenchmarkParams params = runResult.getParams();
            String benchmark = params.getBenchmark();
            String[] items = benchmark.split("\\.");
            String role = items[items.length - 2];
            if (role.endsWith("Benchmark")) {
                role = role.substring(0, role.length() - "Benchmark".length());
            }
            Result primaryResult = runResult.getPrimaryResult();
            return Tuples.of(role, primaryResult.getLabel(), primaryResult.getScore());
        }).collect(groupingBy(Tuple3::_1));


        List<Pair<String, List<Pair<String, Number>>>> data = map.entrySet().stream()
                .map(it -> Pair.of(it.getKey(), Lists.convert(it.getValue(), s -> Pair.of(s._2(), (Number) s._3()))))
                .collect(toList());
        exportToImage(data);
    }

    private void exportToImage(List<Pair<String, List<Pair<String, Number>>>> data) {
        RunResultExporter.data = data;
        Objects.requireNonNull(RunResultExporter.imgPath);
        Objects.requireNonNull(RunResultExporter.title);
        new ImageExporter().run(new String[]{});
    }
}

package net.dongliu.benchmarks.mock;

import java.io.Serializable;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
public class Employee implements Serializable {
    private static final long serialVersionUID = -3445024369955999790L;
    private String name;
    private boolean male;
    private float height;
    private float weight;
    private int age;
    private String educationQualification;
    private String address;

    public String getEducationQualification() {
        return educationQualification;
    }

    public void setEducationQualification(String educationQualification) {
        this.educationQualification = educationQualification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

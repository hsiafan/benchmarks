package net.dongliu.benchmarks.mock;

import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.io.Serializable;
import java.util.List;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
@State(Scope.Benchmark)
public class Company implements Serializable {
    private static final long serialVersionUID = 6897656719610780280L;
    private String name;
    private String address;
    private String description;
    private List<Employee> employees;
    private int year;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

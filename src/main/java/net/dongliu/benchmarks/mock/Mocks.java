package net.dongliu.benchmarks.mock;

import net.dongliu.commons.collection.Lists;
import net.dongliu.commons.reflect.Beans;

import java.util.List;
import java.util.Map;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
public class Mocks {

    public static final Company company = mockCompany();
    public static final Map<String, Object> map = mockMap();
    static int count;

    public static void consume(Object o) {
        if (o != null) {
            count++;
        }
    }

    public static Employee mockEmployee() {
        Employee employee = new Employee();
        employee.setName("苗翠花");
        employee.setMale(false);
        employee.setAge(28);
        employee.setHeight(167.5f);
        employee.setWeight(105f);
        employee.setEducationQualification("硕士研究生");
        employee.setAddress("北京市海淀区苏州街20号院银丰大厦2号楼");
        return employee;
    }

    public static Company mockCompany() {
        Company company = new Company();
        company.setName("北京天海工业有限公司");
        company.setAddress("北京市海淀区苏州街20号院银丰大厦2号楼");
        company.setYear(1998);
        company.setDescription("京东JD.COM-专业的综合网上购物商城,在线销售家电、数码通讯、电脑、家居百货、服装服饰、母婴、图书、食品、在线旅游等数万个品牌千万种优质商品。");
        company.setEmployees(Lists.of(mockEmployee(), mockEmployee(), mockEmployee(), mockEmployee(), mockEmployee(),
                mockEmployee(), mockEmployee(), mockEmployee(), mockEmployee(), mockEmployee()));
        return company;
    }

    static Map<String, Object> mockMap() {
        Company company = Mocks.company;
        Map<String, Object> map = Beans.toMap(company);
        List<Map<String, Object>> list = Lists.convert(company.getEmployees(), Beans::toMap);
        map.put("employees", list);
        return map;
    }

}

package net.dongliu.benchmarks.serialize;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import net.dongliu.benchmarks.mock.Mocks;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
public class HessianBenchmark {

    @Benchmark
    public void serialize() throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Hessian2Output out = new Hessian2Output(bos);
        out.startMessage();
        out.writeInt(2);
        out.writeObject(Mocks.company);
        out.completeMessage();
        out.close();
        byte[] data = bos.toByteArray();

        Mocks.consume(data);
    }

    @Benchmark
    public void deSerialize() throws Exception {
        ByteArrayInputStream bin = new ByteArrayInputStream(SerializeHelper.hessianBytes);
        Hessian2Input in = new Hessian2Input(bin);
        in.startMessage();
        Mocks.consume(in.readObject());
        in.completeMessage();
        in.close();
        bin.close();
    }

}

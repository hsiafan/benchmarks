package net.dongliu.benchmarks.serialize;

import net.dongliu.benchmarks.mock.Mocks;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
public class JvmSerializationBenchmark {

    @Benchmark
    public void serialize() throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(Mocks.company);
            Mocks.consume(bos.toByteArray());
        }
    }

    @Benchmark
    public void deSerialize() throws Exception {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(SerializeHelper.jvmBytes);
                ObjectInputStream ois = new ObjectInputStream(bis)) {
            Mocks.consume(ois.readObject());
        }
    }

}

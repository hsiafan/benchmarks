package net.dongliu.benchmarks.serialize;

import com.caucho.hessian.io.Hessian2Output;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dongliu.benchmarks.mock.Company;
import net.dongliu.benchmarks.mock.Mocks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Dong Liu
 */
class SerializeHelper {

    static ObjectMapper mapper = new ObjectMapper();

    public static byte[] jacksonJson = getJacksonJson(Mocks.company);

    public static byte[] jvmBytes = getJvmBytes(Mocks.company);

    public static byte[] hessianBytes = getHessianBytes(Mocks.company);

    private static byte[] getHessianBytes(Company company) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Hessian2Output out = new Hessian2Output(bos);
            out.startMessage();
            out.writeObject(company);
            out.completeMessage();
            out.close();
            return bos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static byte[] getJvmBytes(Company company) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(company);
            return bos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static byte[] getJacksonJson(Object o) {
        try {
            return SerializeHelper.mapper.writeValueAsBytes(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}

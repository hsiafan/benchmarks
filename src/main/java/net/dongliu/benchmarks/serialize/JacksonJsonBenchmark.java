package net.dongliu.benchmarks.serialize;

import net.dongliu.benchmarks.mock.Company;
import net.dongliu.benchmarks.mock.Mocks;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;

import java.io.IOException;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
public class JacksonJsonBenchmark {

    @Benchmark
    public void serialize() throws IOException {
        byte[] bytes = SerializeHelper.mapper.writeValueAsBytes(Mocks.company);
        Mocks.consume(bytes);
    }


    @Benchmark
    public void deSerialize() throws Exception {
        Company company = SerializeHelper.mapper.readValue(SerializeHelper.jacksonJson, Company.class);
        Mocks.consume(company);
    }

}

Java Microbenchmarking with JMH

## Json serialization / deserialization
![Alt Json benchmarks](images/json_benchmarks.png?raw=true "Json benchmarks")

## Java serialization / deserialization
![Alt Serialization benchmarks](images/serialization_benchmarks.png?raw=true "Serialization benchmarks")
